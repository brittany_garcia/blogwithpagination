# blogwithpagination

Video of basic blog pagination working on local:
https://drive.google.com/file/d/1kqvr23RgfgrOBG4YRfyzIR84ITR1SpvD/view


Once installed new pagination gem to use with custom collections, resulted in gem incompatibility errors:
```
`➜  blogwithpagination git:(master) middleman server
Resolving dependencies...
Bundler could not find compatible versions for gem "middleman-core":
  In snapshot (Gemfile.lock):
    middleman-core (= 4.3.11)

  In Gemfile:
    middleman (~> 4.2) was resolved to 4.3.11, which depends on
      middleman-core (= 4.3.11)

    middleman-pagination was resolved to 1.2.0, which depends on
      middleman-core (~> 3.3)

Running `bundle update` will rebuild your snapshot from scratch, using only
the gems in your Gemfile, which may resolve the conflict.`
```

